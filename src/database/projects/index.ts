import { DB } from '../../services/database'

const _f = DB.field
const _v = DB.value

export async function getAll() {
    let data: any = await DB.executeRaw(`SELECT * FROM ${_f('projects')} `)
    return data.length > 0 ? data : false
}