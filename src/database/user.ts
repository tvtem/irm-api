import * as Settings from '../settings';
import { DB } from '../services/database';

export async function findById(id: any): Promise<any> {
    let data = await DB.executeRaw(`SELECT \`id\`,\`email\`,\`name\`,\`role\`,\`profile_pic\` FROM \`users\` WHERE \`id\`='${id}' LIMIT 1`);

    if (!data)
        return false;
    return data;
}

export async function findByUser(mail: any): Promise<any> {
    let data = await DB.executeRaw(`SELECT * FROM \`users\` WHERE \`email\`='${mail}' LIMIT 1`);

    if (!data)
        return false;
    return data;
}


export async function findAllUsers(filter: string): Promise<any> {
    let data = null
    if (filter && filter !== '')
        data = await DB.executeRaw(`SELECT \`id\`,\`email\`,\`name\`,\`role\`,\`profile_pic\` FROM \`users\` WHERE \`name\` LIKE("%${filter}%") OR \`email\` LIKE("%${filter}%")`);
    else
        data = await DB.executeRaw(`SELECT \`id\`,\`email\`,\`name\`,\`role\`,\`profile_pic\` FROM \`users\``);

    if (!data)
        return false;
    return data;
}

export async function getAllRoles(): Promise<any> {
    let data = await DB.executeRaw(`SELECT * FROM \`roles\``);

    if (!data)
        return false;
    return data;
}

export async function updateFields(fields: any, values: any, id: any): Promise<any> {
    let now = Date.now();

    fields.push('updated')
    values.push(now)

    if (values.length < fields.length) return false;

    let withData = 'SET ';
    for (let i = 0; i < fields.length; i++) {
        let commit = i === 0 ? '' : ', ';
        withData = withData + commit + fields[i] + "='" + values[i] + "'";
    }

    let data = await DB.executeRaw(`UPDATE \`accounts\` ${withData} WHERE \`id\`='${id}'`);

    if (!data)
        return false;
    return data;
}

export async function storeToken(id: any, token: any, duration: any, role: number, resolve: any): Promise<any> {
    let now = Date.now();

    return await DB.executeRaw("INSERT INTO \`user_tokens\` (token, user, role, created, till) VALUES ('" + token + "','" + id + "','" + role + "','" + now + "','" + duration + "')");
}

export async function expireToken(user: any, token: any, role: number): Promise<any> {
    return await DB.executeRaw(`UPDATE \`user_tokens\` SET \`till\`='0' WHERE \`user\`='${user}' and \`token\`='${token}' and \`role\`='${role}'`);
}

export async function registerNewUser(name: string, email: string, password: string): Promise<any> {
    let now = Date.now();

    let emailCheck: any = await DB.executeRaw(`SELECT \`email\` FROM \`accounts\` WHERE \`email\`='${email}' `)
    if (emailCheck.length > 0) {
        return 2 // email in use
    }

    let insert: any = await DB.updateFields(
        `accounts`,
        [
            `name`,
            `email`,
            `password`,
            `role`
        ],
        [
            name,
            email,
            password,
            `user`
        ],
        `new`
    )

    return (insert > 0 ? 1 : 0)
}