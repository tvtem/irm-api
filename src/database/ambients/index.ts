import * as Settings from '../../settings';
import { DB } from '../../services/database';

const _f = DB.field
const _v = DB.value

export async function getAmbients(project: number) {
    let data: any = await DB.executeRaw(`SELECT * FROM ${_f('ambients')} WHERE ${_f('project')}=${_v(project)}`)
    return data.length > 0 ? data : false
}

export async function updateAmbient(ambient: number, fields: any, values: any) {
    let data: any = await DB.updateFields('ambients', fields, values, ambient)
    return data ? true : false
}