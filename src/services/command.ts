import * as Settings from '../settings';
var readln = require('readline')
var cl = readln.createInterface( process.stdin, process.stdout );

async function promptInput (prompt:any)
{
    return new Promise( (res, rej) => {
        cl.question(prompt, (input:any) => {
            res(input)
        });
    })
}


export async function start() {
    var cmd;
    while ( true) {
        try
        {
            cmd = await promptInput('APP > ')
            handleCommand(cmd)
        }
        catch (Exc)
        {
            Settings.LOGGER.error(Exc)
            break;
        }
    }

    Settings.LOGGER.info('Command service stopped!');
}

async function handleCommand(command:any){
    switch (command){

        default: {
            //Settings.LOGGER.info('Handling command: ' + command)
            return true
        }

    }
}

