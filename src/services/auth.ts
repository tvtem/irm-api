import * as tokenService from './token';
import * as userRepository from '../database/user';

export async function login(email: string, password: string, res: any, resolve: any, reject: any): Promise<string> {

    let data: any = await userRepository.findByUser(email);
    if (!data || data.length < 1) { return res.json({ error: 'INVALID_LOGIN' }) }

    let userdata: any = data[0];
    if (!userdata || password != userdata.password) { return res.json({ error: 'INVALID_LOGIN' }) }

    let user = {
        id: userdata.id,
        email: userdata.email,
        name: userdata.name,
        role: userdata.role,
        profile_pic: userdata.profile_pic
    };

    let token = await generateToken(user.id, user.role);

    return resolve({
        id: user.id,
        email: user.email,
        name: user.name,
        token: token,
        role: user.role,
        profile_pic: user.profile_pic
    });

}

export async function logout(user: string, token: string, role: string, resolve: any, reject: any): Promise<string> {
    return resolve(false)
}

async function generateToken(user: any, role: any) {
    return tokenService.userToken(user, role);
}
