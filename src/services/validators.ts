import * as Settings from '../settings';

export function validateLogin(req: any) {
    return new Promise<{ email: string, password: string }>((resolve, reject) => {
        try {
            let rd = req.body;
            resolve({ email: rd.email, password: rd.password });
        }
        catch (err) {
            Settings.LOGGER.error(err);
            reject(err)
        }
    });
}

export function validateSignUp(req: any) {
    return new Promise<{ name: string, email: string, password: string }>((resolve, reject) => {
        try {
            let rd = req.body;
            if (rd.name && rd.name.length > 5 &&
                rd.email && rd.email.length > 4 &&
                rd.password && rd.password.length > 3)
                resolve({ name: rd.name, email: rd.email, password: rd.password });
            reject({ message: 'Invalid Data!' })
        }
        catch (err) {
            Settings.LOGGER.error(err);
            reject(err)
        }
    });
}

export function validateLogout(req: any) {
    return new Promise<{ user: any, token: string, role: string }>((resolve, reject) => {
        try {
            let rd = req.body;
            resolve({ user: rd.user, token: rd.token, role: rd.role });
        }
        catch (err) {
            Settings.LOGGER.error(err);
            reject(err)
        }
    });
}


export function validateReqToken(req: any) {
    return new Promise<{ user: any, token: any, role: any }>((resolve, reject) => {
        try {
            let rd = req.query;
            resolve({ user: rd.user, token: rd.token, role: rd.role });
        }
        catch (err) {
            Settings.LOGGER.error(err);
            reject(err)
        }
    });
}


export function validateReq(req: any) {
    return new Promise<{ data: any }>((resolve, reject) => {
        try {
            let rd = req.query;
            resolve({ data: rd });
        }
        catch (err) {
            Settings.LOGGER.error(err);
            reject(err)
        }
    });
}

export function validatePostToken(req: any) {
    return new Promise<{ user: any, token: any, role: any }>((resolve, reject) => {
        try {
            let rd = req.body;
            resolve({ user: rd.user, token: rd.token, role: rd.role });
        }
        catch (err) {
            Settings.LOGGER.error(err);
            reject(err)
        }
    });
}

export function validatePostData(req: any) {
    return new Promise<{ data: any }>((resolve, reject) => {
        try {
            let rd = JSON.parse(req.body.data);
            let _data: any = {}

            for (let key in rd) {
                _data[key] = rd[key]
            }

            resolve({ data: _data });
        }
        catch (err) {
            Settings.LOGGER.error(err);
            reject(err)
        }
    });
}