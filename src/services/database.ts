import * as Settings from '../settings';
var mysql = require('mysql');

let Connection: any = null;

class Database {

    create() {
        let _conn: any = mysql.createPool({
            connectionLimit: 50,
            host: 'localhost',
            user: 'root',
            password: 'test',
            database: 'irm'
        });

        Connection = _conn;

        return this;
    }

    destroy() {

    }

    field(name: string) {
        return `\`${name}\``;
    }

    value(name: any) {
        return `'${name}'`;
    }

    async executeRaw(qry: string) {
        return await new Promise(function (resolve: any, reject: any) {
            Connection.getConnection(function (err: any, connection: any) {
                let result = connection.query(qry, function (err: any, rows: any, fields: any) {
                    if (err) {
                        let e = new Error(err)
                        Settings.LOGGER.error({ msg: e, stack: e.stack });
                        connection.release();
                        reject({ error: true });
                    } else {
                        connection.release();
                        resolve(JSON.parse(JSON.stringify(rows)));
                    }
                });
            });

        });
    }

    async getName(table: string, id: number) {
        const _f = this.field
        const _v = this.value

        return await new Promise(function (resolve: any, reject: any) {
            Connection.getConnection(function (err: any, connection: any) {
                let qry = `SELECT \`name\` FROM  ${_f(table)} WHERE id=${_v(id)} `;
                let result = connection.query(qry, function (err: any, rows: any, fields: any) {
                    if (err) {
                        let e = new Error(err)
                        Settings.LOGGER.error({ msg: e, stack: e.stack });
                        connection.release();
                        resolve('NULL');
                    } else {
                        connection.release();
                        let data = JSON.parse(JSON.stringify(rows))[0];
                        if (data === undefined)
                            resolve('')
                        else {
                            resolve(data.name);
                        }
                    }
                });
            });

        });
    }

    async getLastTableID(table: string) {
        const _f = this.field
        const _v = this.value

        return await new Promise<number>(function (resolve: any, reject: any) {
            Connection.getConnection(function (err: any, connection: any) {
                let qry = `SELECT ${_f('id')} FROM ${_f(table)} ORDER BY ${_f('id')} DESC LIMIT 1`;
                let result = connection.query(qry, function (err: any, rows: any, fields: any) {
                    if (err) {
                        let e = new Error(err)
                        Settings.LOGGER.error({ msg: e, stack: e.stack });
                        connection.release();
                        resolve(0);
                    } else {
                        connection.release();
                        let data = JSON.parse(JSON.stringify(rows))[0];
                        if (data === undefined)
                            resolve(0)
                        else {
                            resolve(data.id);
                        }
                    }
                });
            });

        });
    }

    filterQuery(string: string) {
        let newString: string = `${string}`

        newString = newString.replace(new RegExp(`'`, 'g'), `\\'`)

        return newString
    }

    async updateFields(table: any, fields: any, values: any, id: any, field: any = 'id'): Promise<any> {
        const _f = this.field
        const _v = this.value

        let now = Date.now();
        let data: any = null;

        fields.push('updated')
        values.push(now)

        if (id === "new") {
            fields.push('created')
            values.push(now)

            if (values.length < fields.length) return false;

            let f = '';
            let v = '';

            let ii = 0;
            for (let i = 0; i < fields.length; i++) {
                if (fields[i] !== 'id') {
                    let commit = ii === 0 ? '' : ', ';

                    f = f + commit + '`' + fields[i] + '`';
                    v = v + commit + "'" + this.filterQuery(values[i]) + "'";

                    ii++;
                }
            }

            let withData = `(${f}) VALUES (${v})`;

            let qry = `INSERT INTO ${_f(table)} ${withData}`;
            let exec: any = await DB.executeRaw(qry);
            data = exec.insertId;
        }
        else {

            if (values.length < fields.length) return false;


            let withData = 'SET ';
            for (let i = 0; i < fields.length; i++) {
                let commit = i === 0 ? '' : ', ';
                withData = withData + commit + `\`${fields[i]}\`='` + this.filterQuery(values[i]) + "'";
            }
            let sql = `UPDATE ${_f(table)} ${withData} WHERE ${_f(field)}=${_v(id)}`
            data = await DB.executeRaw(sql);
        }

        if (!data)
            return false;
        return data;
    }
}

let DB = new Database().create();
export { DB }
export default Database;