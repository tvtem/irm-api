const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const API_SECRET = '8v2QWxQ4gdbbujvQda4bq99BiG13nwnH';

const files = new transports.File({ filename: 'api.log' });
const console = new transports.Console();

const myFormat = printf((info: any) => {
    return `${info.timestamp} [${info.level}]: ${info.level === `error` ? `at ${info.message.stack}` : info.message}`;
});

const LOGGER = createLogger({
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [
        console,
        files
    ]
});

export const logError = function (msg: any) {
    let err = new Error(msg)
    LOGGER.error(`${msg}`)
}

export const logInfo = function (msg: any) {
    LOGGER.info(`${msg}`)
}

// Counters
import Counter from './components/counter'
export const NotificationID = new Counter(0)


export { API_SECRET, LOGGER }