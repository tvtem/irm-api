'use strict';
import * as Settings from './settings';

import * as tokenService from './services/token'

// Routes
import * as ImgUploader from './routes/upload/img';
import * as FileUploader from './routes/upload/file';

import * as LoginRoute from './routes/login';

import * as ProjectsRoute from './routes/projects'
import * as AmbientsRoute from './routes/ambients'
import * as ObjectsRoute from './routes/objects';

/////////////////////////////////////////
/*
const https = require("https"),
    fs = require("fs");

const options = {
    key: fs.readFileSync("/etc/nginx/ssl/private.key"),
    cert: fs.readFileSync("/etc/nginx/ssl/certificate.crt")
};*/


const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

ImgUploader.UploadImg(app);
FileUploader.UploadFile(app);


app.post('/login', LoginRoute.login);
app.post('/logout', LoginRoute.logout);

app.get('/projects',
    // tokenService.authorizeToken, 
    ProjectsRoute.getProjectsList);

app.get('/ambients',
    // tokenService.authorizeToken, 
    AmbientsRoute.getAmbientsList);

app.post('/ambients/update',
    // tokenService.authorizeToken, 
    AmbientsRoute.updateAmbient);

app.post('/ambients/object/save', AmbientsRoute.saveAmbientObjects);
app.post('/ambients/object/list', AmbientsRoute.ambientObjectsList);

app.post('/message/find', AmbientsRoute.findMessages);
app.post('/message/update', AmbientsRoute.updateMessages);


app.get('/', (req: any, res: any) => {
    res.json({
        'Welcome': 'Hello, World!'
    });
});

app.post('/objects/save', ObjectsRoute.saveObjects);
app.get('/objects/list', ObjectsRoute.listObjects);

//https.createServer(options, app).listen(3466);
app.listen(3466);

Settings.LOGGER.info('API READY!');
Settings.LOGGER.info('Listening on localhost:3466');

//CommandService.start()