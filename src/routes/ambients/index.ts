import * as ambientRepo from '../../database/ambients'
import { Request, Response } from "express";
import { DB } from '../../services/database';

async function getAmbientsList(req: any, res: any) {
    const id = req.query.project
    return res.json(await ambientRepo.getAmbients(id))
}

async function updateAmbient(req: any, res: any) {
    const { id, name, project, width, height, x, y } = req.body

    const fields = ['id', 'name', 'width', 'height', 'x', 'y']
    const values = [id, name, width, height, x, y]

    return res.json({ sucess: await ambientRepo.updateAmbient(id, fields, values) })
}

const saveAmbientObjects = async (req: Request, res: Response) => {
    const object = req.body;

    try {
        const retorno = await DB.executeRaw(
            `INSERT INTO ambient_objects( id_ambient, id_object, X, Y, flip )
            VALUES(
                ${object.ambient_id}, 
                '${object.id}',
                ${object.X},
                ${object.Y},
                ${object.flip ? 1 : 0}
                )`
        );

        return res.status(200).json(retorno);
    } catch (error) {
        return res.status(400).json(error);
    }
}

const ambientObjectsList = async (req: Request, res: Response) => {
    const { id_ambient } = req.body;

    try {
        const retorno = await DB.executeRaw(`
        SELECT
        X, Y, id_object, img, imgFlipX, imgFlipY, imgHeight, imgWidth, imgX, imgY, size, flip, zIndex
        FROM ambient_objects
        JOIN objects ON objects.id = substring_index(id_object, '-', 1)
        WHERE id_ambient = ${id_ambient}
    `)

        return res.status(200).json(retorno);

    } catch (error) {
        return res.status(400).json(error);
    }
}

const findMessages = async (req: Request, res: Response) => {
    const { idObject } = req.body;

    try {
        const retorno = await DB.executeRaw(`
        SELECT id_ambient,id_object, mensagem, titulo FROM ambient_objects
        WHERE ID_OBJECT = '${idObject}'
    `);

        return res.status(200).json(retorno);

    } catch (error) {
        return res.status(400).json(error);
    }
}

const updateMessages = async (req: Request, res: Response) => {
    const { titulo, mensagem, idObject } = req.body;

    try {
        const retorno = await DB.executeRaw(`
        UPDATE ambient_objects
        SET titulo = '${titulo}', 
        mensagem = '${mensagem}' 
        WHERE id_object = '${idObject}'
    `);

        return res.status(200).json(retorno);

    } catch (error) {
        return res.status(400).json(error);
    }
}

export { getAmbientsList, updateAmbient, saveAmbientObjects, ambientObjectsList, findMessages, updateMessages }