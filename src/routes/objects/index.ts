import { Request, Response } from "express";
import { DB } from '../../services/database';

const saveObjects = async (req: Request, res: Response) => {
    const { object } = req.body;

    try {
        const retorno = await DB.executeRaw(`
        INSERT INTO objects (name, img, imgFlipX, imgFlipY, imgWidth, imgHeight, imgX, imgY, size)
        VALUES (
        '${object.name}', 
        '${object.img}',     
        '${object.imgFlipX}', 
        '${object.imgFlipY}', 
        '${object.imgWidth}', 
        '${object.imgHeight}',
        '${object.imgX}',
        '${object.imgY}',
        '${object.size}'
        )
        `);
        return res.status(201).json(retorno);

    } catch (error) {
        return res.status(400).json(error);
    }
}

const listObjects = async (req: Request, res: Response) => {
    try {
        const retorno = await DB.executeRaw(`
        SELECT * FROM objects
        `);
        return res.status(200).json(retorno);

    } catch (error) {
        return res.status(400).json(error);
    }
}



export { saveObjects, listObjects }