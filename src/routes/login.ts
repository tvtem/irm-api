import * as Settings from '../settings';
import * as authService from '../services/auth';
import { validateLogin, validateLogout } from '../services/validators';
import { NextFunction, Request, Response } from 'express';

export async function login(req: Request, res: Response): Promise<void> {
    try {
        const { email, password } = await validateLogin(req);

        if (!email || !password) { denyAccess(res); return  }

        let auth = await authService.login(email, password, res, function (data: any) {
            Settings.LOGGER.info('===========  LOGIN  =============')
            Settings.LOGGER.info('token: ' + data.token);
            Settings.LOGGER.info('user: ' + data.id);
            Settings.LOGGER.info('email: ' + data.email);
            Settings.LOGGER.info('name: ' + data.name);
            Settings.LOGGER.info('role: ' + data.role);
            Settings.LOGGER.info('==============+++++==============');

            res.json(data);
        }, function (err:any) {
            Settings.LOGGER.error('LOGIN_ERROR: ' + err);
            res.status(401).send();
        });

    } catch (err) {

        Settings.LOGGER.error('msg: ' + err.message);
        res.status(401).send();

    }
}

export async function logout(req: Request, res: Response): Promise<void> {
    try {
        const { user, token, role } = await validateLogout(req);

        if (!user || !token) { denyAccess(res); return  }

        let auth = await authService.logout(user, token, role, function (data: any) {

            Settings.LOGGER.info('===========  LOGOUT  ============');
            Settings.LOGGER.info('user: ' + user);
            Settings.LOGGER.info('token: ' + token);
            Settings.LOGGER.info('Done!');
            Settings.LOGGER.info('==============+++++==============');

            res.json(data);
        }, function (err:any) {
            Settings.LOGGER.error('LOGOUT_ERROR: ' + err);
            res.status(401).send();
        });

    } catch (err) {

        Settings.LOGGER.error('msg: ' + err.message);
        res.status(401).send();

    }
}

export async function denyAccess(res: Response){
    Settings.LOGGER.info('Access Denied.');
    res.status(401).send();
}