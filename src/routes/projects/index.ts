import * as projectRepo from '../../database/projects'

export async function getProjectsList(req: any, res: any) {
    return res.json(await projectRepo.getAll())
}